/* Copyright (C) : 2014-2019
   European Synchrotron Radiation Facility
   BP 220, Grenoble 38043, FRANCE

   This file is part of libhdb++timescale.

   libhdb++timescale is free software: you can redistribute it and/or modify
   it under the terms of the Lesser GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   libhdb++timescale is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the Lesser
   GNU General Public License for more details.

   You should have received a copy of the Lesser GNU General Public License
   along with libhdb++timescale.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef _PQXX_EXTENSION_HPP
#define _PQXX_EXTENSION_HPP

#include <algorithm>
#include <iostream>
#include <pqxx/pqxx>
#include <pqxx/strconv>
#include <vector>
#include <string_view>
#include <cstdint>

#include "TangoValue.hpp"

// why is it OmniORB (via Tango)) and Pqxx define these types in different ways? Perhaps
// its the autotools used to configure them? Either way, we do not use tango, just need its
// types, so undef and allow the Pqxx defines to take precedent
#undef HAVE_UNISTD_H
#undef HAVE_SYS_TYPES_H
#undef HAVE_SYS_TIME_H
#undef HAVE_POLL

#include <tango/tango.h>

// This file conforms to the pqxx style, rather than our own, since it is an extension
// to that project, therefore we have many liniting readability errors raised when
// using clang-tidy. To make our compile clean, we simply disable linting for many lines
// in the file

namespace pqxx
{
    template<>
    inline std::string const type_name<std::uint8_t>{"uint8_t"};

    template<>
    inline std::string const type_name<Tango::DevState>{"Tango::DevState"};

    template<>
    inline std::string const type_name<std::vector<double>>{"std::vector<double>"};

    template<>
    inline std::string const type_name<std::vector<float>>{"std::vector<float>"};

    template<>
    inline std::string const type_name<std::vector<std::int32_t>>{"std::vector<std::int32_t>"};

    template<>
    inline std::string const type_name<std::vector<std::uint32_t>>{"std::vector<std::uint32_t>"};

    template<>
    inline std::string const type_name<std::vector<std::int64_t>>{"std::vector<std::int64_t>"};

    template<>
    inline std::string const type_name<std::vector<std::uint64_t>>{"std::vector<std::uint64_t>"};

    template<>
    inline std::string const type_name<std::vector<std::int16_t>>{"std::vector<std::int16_t>"};

    template<>
    inline std::string const type_name<std::vector<std::uint16_t>>{"std::vector<std::uint16_t>"};

    template<>
    inline std::string const type_name<std::vector<std::uint8_t>>{"std::vector<std::uint8_t>"};

    template<>
    inline std::string const type_name<std::vector<bool>>{"std::vector<bool>"};

    template<>
    inline std::string const type_name<std::vector<std::string>>{"std::vector<std::string>"};

    template<typename T>
    struct nullness<std::vector<T>> : pqxx::no_null<std::vector<T>> {};

    template<typename T>
    struct nullness<hdbpp_internal::TangoValue<T>> : pqxx::no_null<hdbpp_internal::TangoValue<T>> {};

    template<>
    struct nullness<Tango::DevState> : pqxx::no_null<Tango::DevState> {};

// this is an extension to the pqxx strconv types to allow our engine to
// handle vectors. This allows us to convert the value from Tango directly
// into a postgres array string ready for storage
template<typename T>
struct string_traits<hdbpp_internal::TangoValue<T>>
{
public:
    // NOLINTNEXTLINE (readability-identifier-naming)
    static hdbpp_internal::TangoValue<T> from_string(std::string_view str)
    {
        hdbpp_internal::TangoValue<T> value;

        // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (str[0] != '{' || str[str.size() - 1] != '}')
            throw pqxx::conversion_error("Invalid array format");

        std::string_view in = str.substr(1, str.size() - 2);

        // count opening brackets and add one to deduce elements in the string,
        // note we reduce string size to remove the brace at each end
        auto dim_y = std::count(in.begin(), in.end(), '{');

        if(dim_y == 0)
        {
            // Regular array
            auto items = std::count(in.begin(), in.end(), ',');
            value.dim_y = dim_y;
            value.dim_x = items + 1;

            // preallocate all the items in the vector, we can then
            // simply set each in turn
            value.resize(value.dim_x);

            auto element = 0;
            std::string::size_type comma = 0;

            // loop and copy out each value from between the separators
            while ((comma = in.find_first_not_of(',', comma)) != std::string::npos)
            {
                auto next_comma = in.find_first_of(',', comma);
                value[element++] = string_traits<T>::from_string(in.substr(comma, next_comma - comma));
                comma = next_comma;
            }
        }
        else
        {
            // 2D array, or image
            auto items = std::count(in.begin(), in.end(), ',');
            value.dim_y = dim_y;
            value.dim_x = (items + 1) / dim_y;

            // Look for enclosing brackets to extract rows 
            std::string::size_type bracket = 0;
            std::string::size_type close_bracket = 0;

            // loop and copy out each value from between the separators
            while ((bracket = in.find_first_of('{', close_bracket)) != std::string::npos)
            {
                close_bracket = in.find_first_of('}', bracket);

                // We clear the value so we need to extract in another vector
                hdbpp_internal::TangoValue<T> row = string_traits<hdbpp_internal::TangoValue<T>>::from_string(in.substr(bracket, close_bracket - bracket + 1));

                value.insert(value.end(), row.begin(), row.end());
            }
        }
        return value;
    }

    static pqxx::zview to_buf(char *begin, char *end, const hdbpp_internal::TangoValue<T> &value)
    {
        auto* end_ptr = into_buf(begin, end, value);

        return pqxx::zview(begin, end_ptr - begin - 1);
    }

    // NOLINTNEXTLINE (readability-identifier-naming)
    static char *into_buf(char *begin, char *end, const hdbpp_internal::TangoValue<T> &value)
    {
        if (value.empty())
        {
            return begin;
        }

        // simply use the pqxx utilities for this, rather than reinvent the wheel...
        std::string rep;
        if(value.dim_y < 2)
        {
            rep = "{" + separated_list(",", value.begin(), value.end()) + "}";
        }
        else
        {
            // In case of image, unwrap the vector. 

            assert(value.dim_x != 0);
            assert(value.dim_x * value.dim_y <= value.size());

            std::stringstream result;
            result << "{";
            
            result << "{" << separated_list(",", value.begin(), std::next(value.begin(), value.dim_x)) << "}";
            for(std::size_t i = 1; i != value.dim_y; ++i)
            {
                result << ", {" << separated_list(",", std::next(value.begin(), i * value.dim_x), std::next(value.begin(), (i+1) * value.dim_x)) << "}";
            }
            result << "}";
            rep = result.str();
        }
        if(end < begin)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        assert(end >= begin);
        if(static_cast<std::size_t>(end - begin) < rep.size() + 1)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        memcpy(begin, rep.c_str(), rep.size());
        *(begin + rep.size()) = 0;
        return begin + rep.size() + 1;
    }

    static std::size_t size_buffer(const hdbpp_internal::TangoValue<T> &value) noexcept
    {
        if(value.empty())
            return 0;
        return (string_traits<T>::size_buffer(value[0]) * value.size() + 1) * 2;
    }
};

// This specialisation is for string types. Unlike other types the string type requires
// the use of the ARRAY notation and dollar quoting to ensure the strings are stored
// without escape characters.
template<>
struct string_traits<hdbpp_internal::TangoValue<std::string>>
{
public:
    // NOLINTNEXTLINE (readability-identifier-naming)
    static hdbpp_internal::TangoValue<std::string> from_string(std::string_view str)
    {
        hdbpp_internal::TangoValue<std::string> value;

        // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (str[0] != '{' || str[str.size() - 1] != '}')
            throw pqxx::conversion_error("Invalid array format");

        value.dim_x = 0;
        value.dim_y = 0;

        std::pair<array_parser::juncture, std::string> output;

        // use pqxx array parser features to get each element from the array
        array_parser parser(str);
        output = parser.get_next();

        while (output.first != array_parser::juncture::done)
        {
            output = parser.get_next();
            if (output.first == array_parser::juncture::string_value)
            {
                value.push_back(output.second);
                continue;
            }
            if(output.first == array_parser::juncture::row_start)
            {
                ++(value.dim_y);
                continue;
            }
        }
        value.dim_x = value.dim_y == 0 ? value.size() : (value.size() / value.dim_y);
        return value;
    }

    static pqxx::zview to_buf(char *begin, char *end, const hdbpp_internal::TangoValue<std::string> &value)
    {
        auto* end_ptr = into_buf(begin, end, value);

        return pqxx::zview(begin, end_ptr - begin - 1);
    }

    // NOLINTNEXTLINE (readability-identifier-naming)
    static char *into_buf(char *begin, char *end, const hdbpp_internal::TangoValue<std::string> &value)
    {
        // This function should not be used, so we do a simple basic conversion
        // for testing only
        std::string rep = "{" + separated_list(",", value.begin(), value.end()) + "}";
        if(end < begin)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        assert(end >= begin);
        if(static_cast<std::size_t>(end - begin) < rep.size() + 1)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        memcpy(begin, rep.c_str(), rep.size());
        *(begin + rep.size()) = 0;
        return begin + rep.size() + 1;
    }

    static std::size_t size_buffer(const hdbpp_internal::TangoValue<std::string> &value) noexcept
    {
        if(value.empty())
            return 0;

        std::size_t res = 0;

        for(const auto& val : value)
        {
            res += string_traits<std::string>::size_buffer(val) + 1;
        }
        res += 2;
        return res;
    }
};

// This specialisation is for bool, since it is not a normal container class, but
// rather some kind of alien bitfield. We have to adjust the from_string to take into
// account we can not use container element access
template<>
struct string_traits<hdbpp_internal::TangoValue<bool>>
{
public:
    // NOLINTNEXTLINE (readability-identifier-naming)
    static hdbpp_internal::TangoValue<bool> from_string(std::string_view str)
    {
        hdbpp_internal::TangoValue<bool> value;

        // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (str[0] != '{' || str[str.size() - 1] != '}')
            throw pqxx::conversion_error("Invalid array format");

        std::string_view in = str.substr(1, str.size() - 2);
        
        // count opening brackets and add one to deduce elements in the string,
        // note we reduce string size to remove the brace at each end
        auto dim_y = std::count(in.begin(), in.end(), '{');

        if(dim_y == 0)
        {
            // Regular array
            auto items = std::count(in.begin(), in.end(), ',');
            value.dim_y = dim_y;
            value.dim_x = items + 1;

            std::string::size_type comma = 0;

            // loop and copy out each value from between the separators
            while ((comma = in.find_first_not_of(',', comma)) != std::string::npos)
            {
                auto next_comma = in.find_first_of(',', comma);
                // we can not pass an element of the vector, since vector<bool> is not
                // in fact a container, but some kind of bit field. In this case, we
                // have to create a local variable to read the value into, then push this
                // back onto the vector
                bool field = string_traits<bool>::from_string(in.substr(comma, next_comma - comma));
                value.push_back(field);
                comma = next_comma;
            }
        }
        else
        {
            // 2D array, or image
            auto items = std::count(in.begin(), in.end(), ',');
            value.dim_y = dim_y;
            value.dim_x = (items + 1) / dim_y;

            // Look for enclosing brackets to extract rows 
            std::string::size_type bracket = 0;
            std::string::size_type close_bracket = 0;

            // loop and copy out each value from between the separators
            while ((bracket = in.find_first_of('{', close_bracket)) != std::string::npos)
            {
                close_bracket = in.find_first_of('}', bracket);

                // We clear the value so we need to extract in another vector
                hdbpp_internal::TangoValue<bool> row = string_traits<hdbpp_internal::TangoValue<bool>>::from_string(in.substr(bracket, close_bracket - bracket + 1));

                value.insert(value.end(), row.begin(), row.end());
            }
        }
        return value;
    }

    static pqxx::zview to_buf(char *begin, char *end, const hdbpp_internal::TangoValue<bool> &value)
    {
        auto* end_ptr = into_buf(begin, end, value);

        return pqxx::zview(begin, end_ptr - begin - 1);
    }

    // NOLINTNEXTLINE (readability-identifier-naming)
    static char *into_buf(char *begin, char *end, const hdbpp_internal::TangoValue<bool> &value)
    {
        if (value.empty())
        {
            return begin;
        }

        std::string rep;
        if(value.dim_y < 2)
        {
            // simply use the pqxx utilities for this, rather than reinvent the wheel
            rep = "{" + separated_list(",", value.begin(), value.end()) + "}";
        }
        else
        {
            // In case of image, unwrap the vector. 

            assert(value.dim_x != 0);
            assert(value.dim_y * value.dim_x != value.size());

            std::stringstream result;
            result << "{";
            
            result << "{" << separated_list(",", value.begin(), std::next(value.begin(), value.dim_x)) << "}";
            for(std::size_t i = 1; i != value.dim_y; ++i)
            {
                result << ", {" << separated_list(",", std::next(value.begin(), i * value.dim_x), std::next(value.begin(), (i+1) * value.dim_x)) << "}";
            }
            result << "}";
            rep = result.str();
        }
        if(end < begin)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        assert(end >= begin);
        if(static_cast<std::size_t>(end - begin) < rep.size() + 1)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        memcpy(begin, rep.c_str(), rep.size());
        *(begin + rep.size()) = 0;
        return begin + rep.size() + 1;
    }

    static std::size_t size_buffer(const std::vector<bool> &value) noexcept
    {
        if(value.empty())
            return 0;
        return (string_traits<bool>::size_buffer(value[0]) * value.size() + 1) * 2;
    }
};

// This specialisation is for bool, since it is not a normal container class, but
// rather some kind of alien bitfield. We have to adjust the from_string to take into
// account we can not use container element access
template<>
struct string_traits<std::vector<bool>>
{
public:
    // NOLINTNEXTLINE (readability-identifier-naming)
    static std::vector<bool> from_string(std::string_view str)
    {
        std::vector<bool> value;

        // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (str[0] != '{' || str[str.size() - 1] != '}')
            throw pqxx::conversion_error("Invalid array format");

        // not the best solution right now, but we are using this for
        // testing only. Copy the str into a std::string so we can work
        // with it more easily.
        std::string_view in = str.substr(1, str.size() - 2);
        std::string::size_type comma = 0;

        // loop and copy out each value from between the separators
        while ((comma = in.find_first_not_of(',', comma)) != std::string::npos)
        {
            auto next_comma = in.find_first_of(',', comma);

            // we can not pass an element of the vector, since vector<bool> is not
            // in fact a container, but some kind of bit field. In this case, we
            // have to create a local variable to read the value into, then push this
            // back onto the vector
            bool field = string_traits<bool>::from_string(in.substr(comma, next_comma - comma));
            value.push_back(field);

            comma = next_comma;
        }
        return value;
    }

    static pqxx::zview to_buf(char *begin, char *end, const std::vector<bool> &value)
    {
        auto* end_ptr = into_buf(begin, end, value);

        return pqxx::zview(begin, end_ptr - begin - 1);
    }

    static char *into_buf(char *begin, char *end, const std::vector<bool> &value)
    {
        if(value.empty())
        {
            return begin;
        }

        // simply use the pqxx utilities for this, rather than reinvent the wheel
        std::string rep = "{" + separated_list(",", value.begin(), value.end()) + "}";

        if(end < begin)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        assert(end >= begin);
        if(static_cast<std::size_t>(end - begin) < rep.size() + 1)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        memcpy(begin, rep.c_str(), rep.size());
        *(begin + rep.size()) = 0;
        return begin + rep.size() + 1;
    }

    static std::size_t size_buffer(const std::vector<bool> &value) noexcept
    {
        if(value.empty())
            return 0;
        return (string_traits<bool>::size_buffer(value[0]) * value.size() + 1) * 2;
    }
};

// This specialisation is for string types. Unlike other types the string type requires
// the use of the ARRAY notation and dollar quoting to ensure the strings are stored
// without escape characters.
template<>
struct string_traits<std::vector<std::string>>
{
public:
    // NOLINTNEXTLINE (readability-identifier-naming)
    static  std::vector<std::string> from_string(std::string_view str)
    {
        std::vector<std::string> value;

        // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (str[0] != '{' || str[str.size() - 1] != '}')
            throw pqxx::conversion_error("Invalid array format");

        std::pair<array_parser::juncture, std::string> output;

        // use pqxx array parser features to get each element from the array
        array_parser parser(str);
        output = parser.get_next();

        if (output.first == array_parser::juncture::row_start)
        {
            output = parser.get_next();

            // loop and extract each string in turn
            while (output.first == array_parser::juncture::string_value)
            {
                value.push_back(output.second);
                output = parser.get_next();

                if (output.first == array_parser::juncture::row_end)
                    break;

                if (output.first == array_parser::juncture::done)
                    break;
            }
        }
        return value;
    }

    static pqxx::zview to_buf(char *begin, char *end, const std::vector<std::string> &value)
    {
        auto* end_ptr = into_buf(begin, end, value);

        return pqxx::zview(begin, end_ptr - begin - 1);
    }

    static char *into_buf(char *begin, char *end, const std::vector<std::string> &value)
    {
        if(value.empty())
        {
            return begin;
        }

        // simply use the pqxx utilities for this, rather than reinvent the wheel
        std::string rep = "{" + separated_list(",", value.begin(), value.end()) + "}";

        if(end < begin)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        assert(end >= begin);
        if(static_cast<std::size_t>(end - begin) < rep.size() + 1)
        {
            throw pqxx::conversion_overrun("The buffer is too small.");
        }
        memcpy(begin, rep.c_str(), rep.size());
        *(begin + rep.size()) = 0;
        return begin + rep.size() + 1;
    }

    static std::size_t size_buffer(const std::vector<std::string> &value) noexcept
    {
        if(value.empty())
            return 0;

        std::size_t res = 0;

        for(const auto& val : value)
        {
            res += string_traits<std::string>::size_buffer(val) + 1;
        }
        res += 2;
        return res;
    }
};

// Specialization for Tango::DevState, its stored as an init32_t
template<>
struct string_traits<Tango::DevState>
{
    static Tango::DevState from_string(std::string_view Str)
    {
        return static_cast<Tango::DevState>(string_traits<std::int32_t>::from_string(Str));
    }

    static pqxx::zview to_buf(char *begin, char *end, const Tango::DevState &value)
    {
        // DevState is an enum, so its bit of a special case. We simply
        // convert it to an int for storage
        return string_traits<std::int32_t>::to_buf(begin, end, static_cast<std::int32_t>(value));
    }

    static char *into_buf(char *begin, char *end, const Tango::DevState &value)
    {
        // DevState is an enum, so its bit of a special case. We simply
        // convert it to an int for storage
        return string_traits<std::int32_t>::into_buf(begin, end, static_cast<std::int32_t>(value));
    }

    static std::size_t size_buffer(const Tango::DevState &value) noexcept
    {
        // DevState is an enum, so its bit of a special case. We simply
        // convert it to an int for storage
        return string_traits<std::int32_t>::size_buffer(static_cast<std::int32_t>(value));
    }
};
} // namespace pqxx
#endif
