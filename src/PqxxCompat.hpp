#ifndef _PQXX_COMPAT_HPP
#define _PQXX_COMPAT_HPP

#include <exception>
#include <memory>

#include <pqxx/connection>

namespace pqxx_compat
{
    const std::exception& handle_exception(std::exception_ptr eptr);
    void close_connection(const std::shared_ptr<pqxx::connection>& conn);
}

#endif  // _PQXX_COMPAT_HPP
