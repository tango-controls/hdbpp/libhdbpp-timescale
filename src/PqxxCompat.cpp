#include "PqxxCompat.hpp"

#include <exception>
#include <memory>

#include <pqxx/version>
#include <pqxx/except>
#include <pqxx/connection>

namespace pqxx_compat
{
    const std::exception& handle_exception(std::exception_ptr eptr)
    {
        try
        {
            if (eptr)
                std::rethrow_exception(eptr);
        }
#if PQXX_VERSION_MAJOR > 6
        catch(const std::exception& e)
        {
            return e;
        }
#else
        catch (const pqxx::pqxx_exception &ex)
        {
            return ex.base();
        }
#endif
        static const std::runtime_error no_exception{"No exception caught!"};
        return no_exception;
    }

    void close_connection(const std::shared_ptr<pqxx::connection>& conn)
    {
#if PQXX_VERSION_MAJOR > 6
        conn->close();
#else
        conn->disconnect();
#endif
    }
}
