# First try with find_package in config mode
find_package(libpqxx ${libpqxx_FIND_VERSION} CONFIG)

# Try with package config
if(NOT libpqxx_FOUND)
    # Ensure pkg-config is installed 
    find_package(PkgConfig REQUIRED)

    # Now search for the tango.pc file, this is a required dependency
    message(STATUS "Search for libpqxx with package config...")
    pkg_check_modules(PQXX REQUIRED libpqxx>=${libpqxx_FIND_VERSION}
        IMPORTED_TARGET GLOBAL)
    
    if(PQXX_FOUND)
        # Create libraries
        add_library(libpqxx::pqxx_shared ALIAS PkgConfig::PQXX)

        message(STATUS "Configured libpqxx for version ${PQXX_VERSION}")
        set(libpqxx_FOUND TRUE)
    else(PQXX_FOUND)
        if(libpqxx_FIND_REQUIRED)
            message(FATAL "Could not find libpqxx.")
        endif(libpqxx_FIND_REQUIRED)
    endif(PQXX_FOUND)
endif(NOT libpqxx_FOUND)
